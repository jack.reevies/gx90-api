declare module 'node-bignumber' {
  export class Key {
    setPublic(n: string, e: string): void
    encrypt(data: string): string
    n: string
    e: string
  }
}