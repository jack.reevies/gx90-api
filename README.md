# GX90 API

A barebones WIP module for interfacing with a TPLink router running the GX90 firmware.

## Install

`npm i @spacepumpkin/gx90-api`

## Usage

### CLI

This module provides a CLI for convenience and can be invoked with `npx gx90 cli <COMMAND> <OPTS>`.

Opts are given in the following form `--KEY=VALUE` (for example `--password=hunter2`)

Key | Default | Notes
-|-|-
method | local | Authentication method. Supports both local login with "local" (default) or using TPLink ID with "cloud" 
password | <none> | Password to log in with. Either router's admin creds or TPLink ID's password
email | <none> | Your TPLink ID's email address (only used if logging in wth TPLink ID)
host | http://tplinkwifi.net | Hostname of the router. TPLink routers prefer tplinkwifi.net but this might not be accessible if you've got custom DNS settings

### As a module

```typescript
import { login, getDevices } from '@spacepumpkin/gx90-api'

async function start() {
  const creds = await login('password') // Optionally specify the host in the second param (ie, http://192.168.0.1)
  const connectedDevices = await getDevices(creds)
}

start()
```

### login

Logs in to the router using the admin password. Returns a credentials object used by all other methods.

### getDevices

Gets an array of currently connected devices. This is used internally by the router to populate the network map.

## Running tests

Tests are fully mocked out so no real network requests are made

TODO: Add a seperate test suite for interfacing with a real device

`npm test`