import { mock, test } from "node:test"
import { readFileSync } from "fs"
import assert, { fail } from "assert"
import * as RSA from 'node-bignumber'


// Require here is a must over import for mock.method to work
const fetch = require('node-fetch') as typeof import('node-fetch')
const mut = require('..') as typeof import('..')

import loginFormKeysResponse from './fixtures/loginFormKeysResponse.json'
import loginFormAuthResponse from './fixtures/loginFormAuthResponse.json'
import loginFormLoginResponse from './fixtures/loginFormLoginResponse.json'
import getDevicesEncryptedResponse from './fixtures/getDevicesEncryptedResponse.json'

const indexHtml = readFileSync('./test/fixtures/index.html', 'utf8')

const loginRsa = {
  n: '147400152213047990727033136147004346733103031985288458332450458806859242043605003404150174090446171651650480460233560027136327325663456109096372971869531130727197464059178168406922984183008755410378326074461124456947748570215827915280709053787014995116751665099191965014918961216467473409603919228363587361789',
  e: '65537'
}

const authRsa = {
  n: '9458886026029921705825475781487170742549806017331313023304546860089446353269547778556863621031593311182178021489684308458315214135463557819499969131655577',
  e: '65537'
}

test('login', async () => {
  let loginBody: string = ''
  mock.method(fetch, 'default', async (url: string, opts: RequestInit) => {
    if (url === 'http://tplinkwifi.net/cgi-bin/luci/;stok=/login?form=keys') {
      return { json: () => Promise.resolve(loginFormKeysResponse) }
    }

    if (url === 'http://tplinkwifi.net/cgi-bin/luci/;stok=/login?form=auth') {
      return { json: () => Promise.resolve(loginFormAuthResponse) }
    }

    if (url === 'http://tplinkwifi.net/webpages/index.html') {
      return { text: () => Promise.resolve(indexHtml) }
    }

    if (url === 'http://tplinkwifi.net/cgi-bin/luci/;stok=/login?form=login') {
      loginBody = opts.body?.toString()!
      return {
        headers: {
          get: (name: string) => {
            if (name === 'set-cookie') {
              return 'sysauth=07c4890b9301d300a582d4e8a18ee056;path=/cgi-bin/luci'
            }
          }
        },
        json: () => Promise.resolve(loginFormLoginResponse)
      }
    }

    fail(`Unexpected url: ${url} or body: ${opts.body}`)
  })

  const creds = await mut.login('password')

  assert.match(loginBody, /^sign=.+&data=.+$/)
  assert.equal(creds.sessionId, '07c4890b9301d300a582d4e8a18ee056')
  assert.equal(creds.stok, 'e36bcc9268106293749b8bd021946d38')
  assert.equal(creds.aesKey.key, '3136399396688174')
  assert.equal(creds.aesKey.iv, '2749023817744124')
  assert.equal(creds.rsaKey.n.toString(), authRsa.n)
  assert.equal(creds.rsaKey.e.toString(), authRsa.e)
  assert.equal(creds.hash, 'e3274be5c857fb42ab72d786e281b4b8')
  assert.equal(creds.password, 'password')
  assert.equal(creds.seq, 71681446)
})

test('getLoginEncryptKey', async () => {
  mock.method(fetch, 'default', async (url: string, opts: RequestInit) => {
    const expectedUrl = 'http://tplinkwifi.net/cgi-bin/luci/;stok=/login?form=keys'
    const expectedBody = 'operation=read'
    if (url === expectedUrl && opts.body === expectedBody) {
      return { json: () => Promise.resolve(loginFormKeysResponse) }
    }
    fail(`Unexpected url: ${url} or body: ${opts.body}`)
  })

  const key = await mut.getLoginEncryptKey()
  assert.equal(key.n.toString(), loginRsa.n)
  assert.equal(key.e.toString(), loginRsa.e)
})

test('getRsaKeyAndSec', async () => {
  mock.method(fetch, 'default', async (url: string, opts: RequestInit) => {
    const expectedUrl = 'http://tplinkwifi.net/cgi-bin/luci/;stok=/login?form=auth'
    const expectedBody = 'operation=read'
    if (url === expectedUrl && opts.body === expectedBody) {
      return { json: () => Promise.resolve(loginFormAuthResponse) }
    }
    fail(`Unexpected url: ${url} or body: ${opts.body}`)
  })

  const { rsaKey, seq } = await mut.getRsaKeyAndSec()
  assert.equal(rsaKey.n.toString(), authRsa.n)
  assert.equal(rsaKey.e.toString(), authRsa.e)
  assert.equal(seq, '71681446')
})

test('getDevices', async () => {
  mock.method(fetch, 'default', async (url: string, opts: RequestInit) => {
    const expectedUrl = 'http://tplinkwifi.net/cgi-bin/luci/;stok=e36bcc9268106293749b8bd021946d38/admin/smart_network?form=game_accelerator'
    if (url === expectedUrl) {
      return { json: () => Promise.resolve(getDevicesEncryptedResponse) }
    }
    fail(`Unexpected url: ${url} or body: ${opts.body}`)
  })

  const rsaKey = new RSA.Key()
  rsaKey.setPublic(loginFormAuthResponse.data.key[0], loginFormAuthResponse.data.key[1])
  const creds = {
    aesKey: {
      key: '3136399396688174',
      iv: '2749023817744124'
    },
    rsaKey: rsaKey,
    password: 'password',
    seq: '71681446',
    hash: 'e3274be5c857fb42ab72d786e281b4b8',
    sessionId: '07c4890b9301d300a582d4e8a18ee056',
    stok: 'e36bcc9268106293749b8bd021946d38'
  }

  const devices = await mut.getDevices(creds)

  assert.equal(devices.length, 49)
})