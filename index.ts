#!/usr/bin/env node
import * as RSA from 'node-bignumber'
import { createCipheriv, createDecipheriv, createHash } from 'crypto'
import fetch from 'node-fetch'

export interface Device {
  totalTime: number
  trafficUsage: number
  deviceType: string
  totalGameTime: number
  deviceName: string
  key: string
  uploadSpeed: number
  onlineTime: number
  isGaming: number
  downloadSpeed: number
  enablePriority: boolean
  remainTime: number
  ip: string
  currentGameTime: number
  signal: number
  index: number
  txrate: number
  mac: string
  rxrate: number
  timePeriod: number
  deviceTag: string
  latency: number
}

export type AES = {
  key: string
  iv: string
}

export type Credentials = {
  aesKey: AES
  rsaKey: RSA.Key
  password: string
  seq: string
  hash: string
  sessionId: string
  stok: string
}

let host = 'http://tplinkwifi.net'
let tHash = '6ee9015e'

function decryptResponse(aes: AES, encryptedResponse: string) {
  const iv = Buffer.from(aes.iv, 'utf8')
  const key = Buffer.from(aes.key, 'utf8')
  const decipher = createDecipheriv('aes-128-cbc', key, iv)
  let decrypted = decipher.update(encryptedResponse, 'base64', 'utf8')
  decrypted += decipher.final('utf8')
  return JSON.parse(decrypted)
}

function aesEncryptData(aes: AES, data: string) {
  const iv = Buffer.from(aes.iv, 'utf8')
  const key = Buffer.from(aes.key, 'utf8')
  const cipher = createCipheriv('aes-128-cbc', key, iv)
  let encrypted = cipher.update(data, 'utf8', 'base64')
  encrypted += cipher.final('base64')
  return encrypted
}

function getSignatureAes(rsaKey: RSA.Key, aesKey: AES, hash: string, seq: string) {
  const aesString = `k=${aesKey.key}&i=${aesKey.iv}`
  const r = aesString + "&h=" + hash + "&s=" + seq
  for (var n = "", o = 0; o < r.length;) {
    n += rsaKey.encrypt(r.substring(o, o + 53))
    o += 53
  }
  return n
}

function getSignature(rsaKey: RSA.Key, hash: string, seq: string) {
  const r = "h=" + hash + "&s=" + seq
  return rsaKey.encrypt(r)
}

function getHeaders() {
  return {
    "accept": "application/json, text/javascript, */*; q=0.01",
    "accept-language": "en-GB,en;q=0.6",
    "cache-control": "no-cache",
    "content-type": "application/x-www-form-urlencoded",
    "pragma": "no-cache",
    "sec-gpc": "1",
    "x-requested-with": "XMLHttpRequest",
    "Referer": `${host}/webpages/index.html?t=${tHash}`,
    "Referrer-Policy": "strict-origin-when-cross-origin",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36"
  }
}

export async function getLoginEncryptKey(): Promise<RSA.Key> {
  const res = await fetch(`${host}/cgi-bin/luci/;stok=/login?form=keys`, {
    "headers": getHeaders(),
    "body": "operation=read",
    "method": "POST"
  })

  const json = await res.json() as any
  const key = new RSA.Key()
  key.setPublic(json.data.password[0], json.data.password[1])
  return key
}

export async function getRsaKeyAndSec(): Promise<{ rsaKey: RSA.Key, seq: string }> {
  const res = await fetch(`${host}/cgi-bin/luci/;stok=/login?form=auth`, {
    "headers": getHeaders(),
    "body": "operation=read",
    "method": "POST"
  })

  const json = await res.json() as any
  const key = new RSA.Key()
  key.setPublic(json.data.key[0], json.data.key[1])
  return { rsaKey: key, seq: json.data.seq }
}

export function getHash(password: string) {
  return createHash('md5').update(`admin${password}`).digest("hex")
}

async function makeRequest<T>(creds: Credentials, url: string, data: string): Promise<T> {
  const encData = aesEncryptData(creds.aesKey, data)
  const signedData = getSignature(creds.rsaKey, creds.hash, creds.seq + encData.length)

  const res = await fetch(`${host}/cgi-bin/luci/;stok=${creds.stok}/admin/${url}`, {
    "headers": { ...getHeaders(), "cookie": `sysauth=${creds.sessionId}`, },
    "body": `sign=${signedData}&data=${encodeURIComponent(encData)}`,
    "method": "POST"
  })

  const json = await res.json() as any
  return decryptResponse(creds.aesKey, json.data).data as T
}

async function getTHash() {
  const res = await fetch(`${host}/webpages/index.html`, { "headers": getHeaders() })
  const html = await res.text()
  const hash = /\?t=(.+)"/g.exec(html)?.[1]
  tHash = hash || tHash
}

async function tplinkAuth(email: string, password: string) {
  const res = await fetch(`https://eu-eweb.tplinkcloud.com/v2/accounts/login`, {
    "headers": {
      "accept": "application/json, text/javascript, */*; q=0.01",
      "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
      "cache-control": "no-cache",
      "content-type": "application/json;charset=UTF-8",
      "pragma": "no-cache",
      "sec-ch-ua": "\"Not_A Brand\";v=\"8\", \"Chromium\";v=\"120\", \"Brave\";v=\"120\"",
      "sec-ch-ua-mobile": "?0",
      "sec-ch-ua-platform": "\"Windows\"",
      "sec-fetch-dest": "empty",
      "sec-fetch-mode": "cors",
      "sec-fetch-site": "same-origin",
      "sec-gpc": "1",
      "x-requested-with": "XMLHttpRequest",
      "Referer": `https://eu-eweb.tplinkcloud.com/cloud_ui_v2/pages/device/index.html?module=login&page=spfLogin&t=${Date.now()}`,
      "Referrer-Policy": "strict-origin-when-cross-origin"
    },
    "body": JSON.stringify({ "cloudUserName": email, "cloudPassword": password }),
    "method": "POST"
  });

  const json = await res.json() as any

  if (!json?.result?.token) {
    throw new Error(`Login to tplink cloud failed. ${JSON.stringify(json)}`)
  }

  return json.result.token
}

export async function tpLinkLogin(email: string, password: string, hostname: string = 'http://tplinkwifi.net') {
  host = hostname
  await getTHash()

  const tpLinkToken = await tplinkAuth(email, password)
  const data = `operation=login&confirm=true&token=${tpLinkToken}`
  return await doLogin('cloud_login', password, data)
}

function createCredentials(password: string, postLoginKey: { rsaKey: RSA.Key; seq: string; }) {
  return {
    hash: getHash(password),
    rsaKey: postLoginKey.rsaKey,
    password,
    seq: postLoginKey.seq,
    aesKey: {
      key: '3136399396688174',
      iv: '2749023817744124'
    }
  } as Credentials
}

async function doLogin(type: string, password: string, payload: string) {
  const postLoginKey = await getRsaKeyAndSec()
  const creds = createCredentials(password, postLoginKey)

  const encData = aesEncryptData(creds.aesKey, payload)
  const signedData = getSignatureAes(creds.rsaKey, creds.aesKey, creds.hash, creds.seq + encData.length)

  const res = await fetch(`${host}/cgi-bin/luci/;stok=/login?form=${type}`, {
    "headers": getHeaders(),
    "body": `sign=${signedData}&data=${encodeURIComponent(encData)}`,
    "method": "POST"
  })

  const json = await res.json() as any
  const decryptedJson = decryptResponse(creds.aesKey, json.data)

  const setCookie = res.headers.get('set-cookie')
  const sessionId = setCookie?.split(';')[0].split('=')[1]

  if (!decryptedJson.success) {
    throw new Error(`Login failed. ${JSON.stringify(decryptedJson)}`)
  }

  if (!sessionId) {
    throw new Error(`Login failed. Could not get session id from cookies`)
  }

  creds.stok = decryptedJson.data.stok
  creds.sessionId = sessionId

  return creds
}


export async function login(password: string, hostname: string = 'http://tplinkwifi.net') {
  host = hostname
  await getTHash()

  const loginKey = await getLoginEncryptKey()

  const rsaPassword = loginKey.encrypt(password)
  const data = `password=${rsaPassword}&confirm=true&operation=login`

  return await doLogin('login', password, data)
}

export async function getDhcpLeases(creds: Credentials) {
  return await makeRequest<any[]>(creds, 'dhcps?form=client', 'operation=load')
}

export async function getDevices(creds: Credentials) {
  return await makeRequest<Device[]>(creds, 'smart_network?form=game_accelerator', 'operation=loadDevice')
}

async function cli() {
  const email = process.argv.find(arg => arg.startsWith('--email='))?.split('=')[1] || ''
  const password = process.argv.find(arg => arg.startsWith('--password='))?.split('=')[1] || 'admin'
  const command = process.argv[3]
  const hostname = process.argv.find(arg => arg.startsWith('--host='))?.split('=')[1] || 'http://tplinkwifi.net'
  const method = process.argv.find(arg => arg.startsWith('--method'))?.split('=')[1] || 'local'

  const creds = method === 'cloud' ? await tpLinkLogin(email, password) : await login(password, hostname)

  switch (command) {
    case 'dhcp':
      const leases = await getDhcpLeases(creds)
      console.log(leases)
      break
    case 'devices':
      const devices = await getDevices(creds)
      console.log(devices)
      break
    default:
      console.log('Unknown command')
  }
}

if (process.argv[2] === 'cli') {
  cli()
}
